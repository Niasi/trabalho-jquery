$(document).ready(function(){
    $('#destination').on('submit', function(event) {
        event.preventDefault();
        $.post($(this).attr('action'), {
            destination: {
                name: $('#form-name').val(), 
                description: $('#form-description').val(), 
                more_info: $('#form-info').val(), 
                price: $('#form-price').val() 
            },
            success: (function() {
                alert("Destination: " + $('#form-name').val() +"\nSuccessfully added.")
            })
        });
    });
    $('button.get').on('click', function(){
        $.get('https://jquery-19-1.herokuapp.com/destinations', function(destination) {
            $.each(destination, function(key, value) {
                $('#destinations').append('<ul><li>Name: ' + value.name + 
                    '</li><li>Description: ' + value.description + 
                    '</li><li>More info: ' + value.more_info + 
                    '</li><li>Price: $' + value.price + '</li></ul>')
            });
        });
    });
});