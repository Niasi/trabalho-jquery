$(document).ready(function () {
    $('.square').on('click', function () {
        if ($(this).css('width') == '100px') {
            $(this).animate({
                width: '250px',
                height: '250px'
            });
            $(this).parent().animate({
                marginTop: '15%'
            });
        } else {
            $(this).animate({
                width: '100px',
                height: '100px'
            });
            $(this).parent().animate({
                marginTop: '20%'
            });
        }
        $(this).toggleClass('big')
    });
});